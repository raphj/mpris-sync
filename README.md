# MPRIS-Sync - Sync audio/video players across the network

Ever wanted to watch a movie or the episode of a series with a friend or a
family member who is not at the same place at you?

MPRIS-Sync aims at solving this situation.

## Presentation

This small script allows playing the same thing at the same time on two players.

One computer acts as a server, two with the other computer connects.
The script then monitors events like playing, pausing and seeking from players
on both sides and tells the other end that this happened.

Of course, transmitting a message over the network takes time, so when playing
or seeking the video, MPRIS-Sync pauses the video or the audio track being
played, and makes sure to resume both players at the same time. Life is not
perfect, it will not be at the same exact atomic time unit, but while watching
an episode with someone else, we didn't notice any latency between two different
cities, so it should be good enougth. Otherwize, please be so kind to report a
bug :-).

## Requirement

You need:

- to use GNU/Linux on both sides (or any OS where D-BUS and MPRIS work, which
  probably includes BSD*),
- have Python 3.6 or later
- and use an MPRIS-compatible player.

In theory, any MPRIS-compatible player should work.
In practice, I had issues with some players including Clementine, VLC and the
Gnome player, even though VLC was the player I originally targeted.
They all probably have slightly different implementations of the MPRIS protocol
and small quirks that probably don't usually matter, but that matter for us.
I recommend using MPV or Gnome-MPV, with which MPRIS-Sync works perfectly well.

## Installation

### Depdencies

First, install the `mpris2` module for `python3`

On Debian, Ubuntu, Mint:

    sudo apt install python3-mpris2

On other distributions:

    sudo pip3 install mpris2

(this may require installing some other dependencies)

### Download MPRIS-Sync

Download `mprissync.py` somewhere. You can clone the repository if you have git.
Be sure to either make it executable, or run it by prefixing the commands given
in this README by `python3 `.

## Usage

### On the server side

On one computer, run the player to which MPRIS-Sync should connect, and run
MPRIS-Sync as the server (change 8080 by the port you want to use):

    ./mprissync.py --listen 0.0.0.0 8080

If you are running several players, MPRIS-Sync will ask you to choose which one
to use.

Note: If you are behind a firewall (say, a modem on a domestic connection), you
will need to allow the given port / enable port forwarding.

Note: `0.0.0.0` means: allow connections from anywhere, on IPv4. I haven't tested
on IPv6. IPv6 probably needs a small patch (in the `get_server_socket` function,
`AF_INET` will probably need to be changed to `AF_INET6`. Patches to cleanly
support IPv6 welcomed).

### On the client side

Run (by replacing `X.Y.Z.A` by the IP address of the server):

    ./mprissync.py --connect X.Y.Z.A 8080

### More (like selecting the player using command parameters)

    ./mprissync.py --help

## How does this work

MPRIS-Sync uses MPRIS to control players. MPRIS is an DBUS API that allows
controlling pretty much any player on GNU/Linux distributions.

First, MPRIS-Sync on both side connect to each other and measure the round-time
trip between the two computers and exchange their current date / time so they
can send meaningful dates at which to resume playing to the other end. Then:

- when someone pauses the player, the position is sent to the other end and the
  other is paused and then the video is seeked to the same position so both
  layers are pause on the same frame. This is useful if you want to show
  something precise to the other user.
- when someone changes the position of or resumes the track, the player is
  paused, the position is sent to the other instance, and both players are
  resumed from the same frame at the same time (see `pause_and_restore()` and
  `play_at()` in the codeif you want to know exactly how it works).
- when someone stops the player, the other player is stopped too.

## Limitations, caveats, area of improvements

There are many of them.

### Exclusive support for GNU/Linux

MPRIS-Sync only supports GNU/Linux (or BSD*), because this OS provides an easy
to use interface to control players.
It should be possible to write a compatible implementation for Android since
KDEConnect is able to control players on Android.

On other OSes like Windows, iOS or macOS, some investigation needs to be done to
know what interface it would be possible to use.

### Track change needs to be manually handled

In a shared playlist (both players are playing the same tracks in the same
order), when the next track / video is played, one of the user needs to pause
and resume the track to ensure both ends are still in sync.

Users must ensure they are playing the same track, track change is not handled
in MPRIS Sync just yet but I don't see any major reason this cannot be
implemented other than lazyness on my side.

### Close the player and you are in to setup everything again

If one end ever close the player or stops MPRIS-Sync, MPRIS-Sync stops working
completely and both ends and the setup needs to be done again. To tackle this
issue, at least two or three things need to be done:

- handle auto-reconnects from both server side and the client side
- automatically reconnect to the player when it is closed and re-opened again
- in the meantime, maybe suggest using another player, without breaking the
  connection.

### Not user friendly

This is broad, but mainly:

- there is no installer
- the program needs to be used from a terminal
- the user on the server side needs to fiddle with firewall / NAT configuration

There should be a GUI to drive MPRIS-Sync. That could be a player plugin, or a
dedicated UI.

And there should be a way to avoid requiring fiddling with firewall/NAT
configuration. This probably requires a server in the middle, or at least some
mechanism to cheat the NAT using some kind of
[reverse connection](https://en.wikipedia.org/wiki/Reverse_connection).

### No support for more than two users

And this would be a cool and useful feature to have and to implement.

### License

This is free software, under AGPLv3 or later.

This means that you can run, study, modify and redistribute your modifications
for any purpose, provided you provide the source code to anybody who uses this
program, under the same terms. This also means that if you build a service out
of this program, you need to provide the source code to your users.

Patches welcome :-)
