#!/usr/bin/env python3

# Copyright (C) 2020 Raphaël Jakse <raphael.mprissync@jakse.fr>
#
# @licstart
# This file is part of MPRIS-Sync.
#
# MPRIS-Sync is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License (GNU AGPL)
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# MPRIS-Sync is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with MPRIS-Sync.  If not, see <http://www.gnu.org/licenses/>.
# @licend

from time import sleep

try:
    from time import monotonic_ns
except:
    from time import monotonic
    def monotonic_ns():
        return int(monotonic() * 1e9)

import mpris2
from dbus.mainloop.glib import DBusGMainLoop
import gi.repository.GLib
import json
import socket
import sys
import signal
#import select

# The number of second after which a keep-alive packet will be sent
# to avoid connections to drop

SOCKET_KEEPIDLE_SEC = 30

def handler_stop_signals(signum, frame):
    sys.exit(0)

signal.signal(signal.SIGINT, handler_stop_signals)
signal.signal(signal.SIGTERM, handler_stop_signals)

def h(exit_code):
    print("""MPRIS Sync. Usage:
    --listen HOST PORT
        Start a server at the given port on the given host
    --connect HOST PORT
        Connect to a server at the given port at the given port
    --player URI (optional)
        Connect to the player that has this DBUS URI. If not provided, mprisync will:
         - connect to the only running player if only one player is running
         - ask you interactively to choose a player""")
    sys.exit(0)

if len(sys.argv) == 1 or sys.argv[1] in ["-h", "--help", "-help", "help"]:
    h(0)

usleep = lambda x: sleep(x/1000000.0)

#https://stackoverflow.com/questions/6833881/python-socket-sending-and-receiving-16-bytes
def socket_read_all(sock, size):
    data = b""
    while len(data) < size:
        more = sock.recv(size - len(data))
        if not more:
            raise EOFError()
        data += more
    return data

def socket_send(sock, msg):
    b = bytes(json.dumps(msg), encoding='utf-8')
    sock.sendall(len(b).to_bytes(4, byteorder='big') + b)

def sync_times(sock, is_server, call_reverse=True):
    min_offset_time = float('inf')
    max_rtt = 0

    if is_server:
        # Without this first exchange, the first result is garbage
        socket_send(sock, ("initsync",))
        msg = get_socket_message(sock)
        if msg[0] != "initsyncok":
            raise Exception("expected initsyncok message")

        nb = 10
        while nb:
            nb -= 1
            time_sent = monotonic_ns()
            socket_send(sock, ("synctime", nb))
            msg = get_socket_message(sock)
            time_recv = monotonic_ns()
            rtt = time_recv - time_sent
            if msg[0] != "syncresp":
                raise Exception("expected syncresp message")
            distant_time = msg[1]
            offset_time = int(distant_time - (time_sent + (rtt / 2)))
            if abs(offset_time) < abs(min_offset_time):
                min_offset_time = offset_time
            max_rtt = max(rtt, max_rtt)
    else:
        msg = get_socket_message(sock)
        if msg[0] != "initsync":
            raise Exception("expected initsync message")
        socket_send(sock, ("initsyncok",))

        cont = True
        while cont:
            msg = get_socket_message(sock)
            socket_send(sock, ("syncresp", monotonic_ns()))
            cont = msg[1]

    ret = sync_times(sock, not is_server, False) if call_reverse else None

    if is_server:
        return (min_offset_time, max_rtt)

    return ret

def get_server_socket(host, port):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_KEEPALIVE, 1)
    sock.setsockopt(socket.IPPROTO_TCP, socket.TCP_KEEPIDLE, SOCKET_KEEPIDLE_SEC)

    sock.bind((sys.argv[2], port))

    print("Waiting a client on " + sys.argv[2] + ":" + str(port))

    sock.listen()
    client, address = sock.accept()
    print(str(address) + " connected")
    return client

def get_client_socket(host, port):
    print("Connecting to " + host + ":" + str(port))

    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect((host, port))
    print("Connected")
    return sock

def play_at(position, play_time):
    global player, ignore_status, last_seeked

    if str(player.PlaybackStatus) == "Playing":
        ignore_status.add('Paused')
        player.Pause()

    if position is not None:
        player.SetPosition(player.Metadata["mpris:trackid"], position)

    usleep_time = ((play_time - monotonic_ns()) / 1000.0) - 100_000
    if usleep_time > 0:
        usleep(usleep_time)

    while play_time > monotonic_ns():
        pass

    ignore_status.add('Playing')
    last_seeked = monotonic_ns()
    player.Play()

def handle_socket_message(msg):
    global player, ignore_status

    if msg[0] == "play":
        play_at(msg[1], msg[2])
    elif msg[0] == "pause":
        if player.PlaybackStatus == "Playing":
            ignore_status .add("Paused")
            player.Pause()

        player.SetPosition(player.Metadata["mpris:trackid"], msg[1])
    elif msg[0] == "stop":
        ignore_status .add("Stopped")
        player.Stop()

def get_socket_message(sock):
    msgsize = int.from_bytes(socket_read_all(sock, 4), byteorder='big')
    return json.loads(str(socket_read_all(sock, msgsize), encoding="utf-8"))

def handle_socket_messages(sock, *args):
    handle_socket_message(get_socket_message(sock))
    return True

def get_player():
    players_uri = list(mpris2.get_players_uri())

    if len(players_uri) == 0:
        print("Could not find any player, sorry")
        return None

    if len(players_uri) == 1:
        player_uri = players_uri[0]
    else:
        print("Several players are running, which one do you want to use?")

        player_uri = ""
        while not player_uri:
            i = 1

            for player_uri in players_uri:
                print(i, " - ", player_uri)
                i += 1

            try:
                player_uri = players_uri[int(input("Type the number of the player: ")) - 1]
            except:
                if player_uri is None:
                    return None

                print("Sorry, we did not understand your answer. Please retry.")

    print("Connecting to player", player_uri)
    return mpris2.Player(dbus_interface_info={'dbus_uri': player_uri})

ignore_status = set()

def pause_and_restore():
    global sock, ignore_status, measured_rtt

    position = int(player.Position)

    play_time = monotonic_ns() + max(measured_rtt * 2, int(1e9))
    distant_play_time = play_time + measured_offset
    socket_send(sock, ("play", position, distant_play_time))
    play_at(position, play_time)

def player_handler(self, obj, *args, **kw):
    global player
    global ignore_status

    if "PlaybackStatus" in obj:
        playbackStatus = str(obj["PlaybackStatus"])

        if playbackStatus in ignore_status:
            ignore_status.remove(playbackStatus)
            return

        if playbackStatus == "Playing":
            pause_and_restore()
        elif playbackStatus == "Paused":
            if str(player.PlaybackStatus) == "Playing":
                return # quirk?
            socket_send(sock, ("pause", int(player.Position)))
        elif playbackStatus == "Stopped":
            socket_send(sock, ("stop",))

def seeked_handler(self, *args, **kw):
    global player, ignore_status, last_seeked

    if monotonic_ns() - last_seeked < 0.5 * 1e9:
        return

    position = int(player.Position)

    if str(player.PlaybackStatus) == "Playing":
        pause_and_restore()
    #else:
        #socket_send(sock, ("pause", int(player.Position)))

DBusGMainLoop(set_as_default=True)

method = None
player_uri = None
player = None

i = 1
while i < len(sys.argv):
    if sys.argv[i] in ("--listen", "--connect"):
        if len(sys.argv) - i < 3:
            print("Wrong number of arguments for --connect")
            h(1)

        method = sys.argv[i]
        host = sys.argv[i + 1]
        port = int(sys.argv[i + 2])
        i += 3
    elif sys.argv[i] == "--player":
        if len(sys.argv) - i < 2:
            print("Wrong number of arguments for --player")
            h(1)
        player_uri = sys.argv[i + 1]
        print("Connecting to player", player_uri)
        player = mpris2.Player(dbus_interface_info={'dbus_uri': player_uri})
        if player == None:
            print("Could not find player " + player_uri)
            sys.exit(1)

        i += 2
    else:
        print("Unrecognized argument " + sys.argv[i])
        h(1)

if not method:
    print("Missing connection method")
    h(1)

if not player:
    player = get_player()

    if not player:
        sys.exit()

last_seeked = 0

player.PropertiesChanged = player_handler
player.Seeked = seeked_handler
sock = get_server_socket(host, port) if method == "--listen" else get_client_socket(host, port)

(measured_offset, measured_rtt) = sync_times(sock, method == "--listen")
print("Measured round trip time: ", measured_rtt / 1e6, "ms")
if not sock:
    print("Something went wrong.")
    sys.exit()

mloop = gi.repository.GLib.MainLoop()
gi.repository.GLib.io_add_watch(sock, gi.repository.GLib.IO_IN | gi.repository.GLib.IO_HUP, handle_socket_messages)
mloop.run()
